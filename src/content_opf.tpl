<?xml version="1.0" encoding="UTF-8"?>
<package
  unique-identifier="pub-identifier"
  version="3.0"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:dcterms="http://purl.org/dc/terms/"
  xmlns="http://www.idpf.org/2007/opf">

  <metadata>
    <dc:identifier id="pub-identifier">randomid-idp0</dc:identifier>
    <meta id="meta-identifier" property="dcterms:identifier">randomid-idp0</meta>

    <dc:title id="pub-title">성경</dc:title>
    <meta id="meta-title" property="dcterms:title">성경</meta>

    <dc:language id="pub-language">kr</dc:language>
    <meta id="meta-language" property="dcterms:language">kr</meta>

    <meta property="dcterms:modified">2015-05-21T02:56:08Z</meta>

    <dc:publisher>Joo</dc:publisher>
    <meta property="dcterms:publisher">Joo</meta>

    <dc:date>2014-11-01</dc:date>
    <meta property="dcterms:date">2014-11-01</meta>

    <dc:description>성경 - 하나님은 모든 사람이 구원을 받으며 진리를 아는 데에 이르기를 원하시느니라 (딤전 2:4)</dc:description>
    <meta property="dcterms:description">성경 - 하나님은 모든 사람이 구원을 받으며 진리를 아는 데에 이르기를 원하시느니라 (딤전 2:4)</meta>

    <dc:creator>GOD</dc:creator>
    <meta property="dcterms:creator">GOD</meta>

    <dc:subject>Religion &amp; Spirituality</dc:subject>
    <meta property="dcterms:subject">Religion &amp; Spirituality</meta>

  </metadata>

  <manifest>
    <item href="toc.html" id="nav" properties="nav" media-type="application/xhtml+xml" />
    <item href="toc.ncx" id="toc.ncx" media-type="application/x-dtbncx+xml"/>
    <item href="style/epub.css" id="epub-css" media-type="text/css"/>
    <item href="style/index.css" id="index-css" media-type="text/css"/>

    {% for bible in bibles %}
    <item href="contents/{{ bible['vcode'] }}_{{ bible['bcode'] }}_index.html" id="{{ bible['vcode'] }}_{{ bible['bcode'] }}_index" media-type="application/xhtml+xml"/>
    <item href="contents/{{ bible['vcode'] }}_{{ bible['bcode'] }}.html" id="{{ bible['vcode'] }}_{{ bible['bcode'] }}" media-type="application/xhtml+xml"/>
    {% endfor %}
  </manifest>
  <spine toc="toc.ncx">

    {% for bible in bibles %}
    <itemref idref="{{ bible['vcode'] }}_{{ bible['bcode'] }}_index"/>
    <itemref idref="{{ bible['vcode'] }}_{{ bible['bcode'] }}"/>
    {% endfor %}
  </spine>
</package>
