import logging
import os
import sqlite3
import zipfile
from jinja2 import Template, Environment, FileSystemLoader

def main():
    logging.basicConfig(level=logging.DEBUG)

    db_file = "%s/../data/holybible.db" % os.path.dirname(os.path.abspath(__file__))
    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    bibles = loadBibles(c, 'GAE')
    makeContentOpf(bibles)
    makeToc('GAE', bibles)
    makeBibles(bibles, c)
    zip("%s/../build" % os.path.dirname(os.path.abspath(__file__)), "holybible_GAE")


def loadBibles(c, vcode):
    bibles = []
    for row in c.execute("select vcode, bcode, name, type, chapter_count from bibles where vcode=?", (vcode,)):
        bible = {
            'vcode': row[0],
            'bcode': row[1],
            'name': row[2],
            'type': row[3],
            'chapter_count': row[4]
        }
        bibles.append(bible)

    return bibles

def makeContentOpf(bibles):
    logging.info("makeContentOpf start")
    base_dir = os.path.dirname(os.path.abspath(__file__))
    env = Environment(loader=FileSystemLoader([base_dir]))

    file = open(base_dir + "/../build/OEBPS/content.opf", "w")
    file.write(env.get_template('content_opf.tpl').render(bibles=bibles).encode("utf-8"))
    logging.info("makeContentOpf done")

def makeToc(vcode, bibles):
    logging.info("makeToc start")
    base_dir = os.path.dirname(os.path.abspath(__file__))
    env = Environment(loader=FileSystemLoader([base_dir]))

    file = open(base_dir + "/../build/OEBPS/toc.html", "w")
    file.write(env.get_template('toc.tpl').render(vcode=vcode, bibles=bibles).encode("utf-8"))

    file = open(base_dir + "/../build/OEBPS/toc.ncx", "w")
    file.write(env.get_template('toc.ncx.tpl').render(vcode=vcode, bibles=bibles).encode("utf-8"))
    logging.info("makeToc done")


def makeBibles(bibles, c):
    logging.info("makeBibles start")
    base_dir = os.path.dirname(os.path.abspath(__file__))
    env = Environment(loader=FileSystemLoader([base_dir]))

    for bible in bibles:
        logging.info("- make file : " + bible['name'] + " start")
        file = open(base_dir + "/../build/OEBPS/contents/" + bible['vcode'] + "_" + str(bible['bcode']) + "_index.html", "w")
        file.write(env.get_template('content_index.tpl').render(title=bible['name'], id=bible['vcode'] + "_" + str(bible['bcode']), chapter_count=bible['chapter_count']).encode("utf-8"))

        file = open(base_dir + "/../build/OEBPS/contents/" + bible['vcode'] + "_" + str(bible['bcode']) + ".html", "w")
        contentList = loadConentList(c, bible)
        file.write(env.get_template('content.tpl').render(title=bible['name'], id=bible['vcode'] + "_" + str(bible['bcode']), list=contentList).encode("utf-8"))
        logging.info("- make file : " + bible['name'] + " done")

    logging.info("makeBibles done")


def loadConentList(c, bible):
    list = []

    for cnum in range(1, bible['chapter_count'] + 1):
        item = {
            'title': cnum,
            'verses': []
        }

        for row in c.execute("select vcode, bcode, cnum, vnum, content from verses where vcode=? and bcode=? and cnum=?", (bible['vcode'], bible['bcode'], cnum)):
            verse = {
                'vcode': row[0],
                'bcode': row[1],
                'cnum': row[2],
                'vnum': row[3],
                'content': row[4]
            }
            item['verses'].append(verse)

        list.append(item)

    return list

def zip(src, dst):
    logging.info("zip start")
    zf = zipfile.ZipFile("%s.epub" % (dst), "w")
    abs_src = os.path.abspath(src)
    for dirname, subdirs, files in os.walk(src):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            arcname = absname[len(abs_src) + 1:]
            logging.info('zipping %s as %s' % (os.path.join(dirname, filename), arcname))
            zf.write(absname, arcname)
    zf.close()
    logging.info("zip done")

if __name__ == '__main__':
    main()
