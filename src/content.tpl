<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html xmlns:epub="http://www.idpf.org/2007/ops" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>{{ title }}</title>
  <link rel="stylesheet" type="text/css" href="../style/epub.css" />
</head>

<body data-type="book">
  <section data-type="content" data-pdf-bookmark="{{ title }}">
    <div class="content" id="{{ id }}">
    {% for item in list %}
      <h2 data-type="chapter" id="chapter{{ item.title }}">{{ item.title }}</h2>
      <ol data-type="verse">
      {% for verse in item.verses %}
        <li>{{ verse.content }}</li>
      {% endfor %}
      </ol>
    {% endfor %}
    </div>
  </section>
</body>

</html>
