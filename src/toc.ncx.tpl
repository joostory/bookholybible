<?xml version="1.0" encoding="UTF-8"?>
<ncx version="2005-1" xmlns="http://www.daisy.org/z3986/2005/ncx/">
  <head>
    <meta content="cover" name="cover"/>
    <meta content="randomid-idp0" name="dtb:uid"/>
  </head>
  <docTitle>
    <text>성경</text>
  </docTitle>
  <navMap>

    {% for bible in bibles %}
    <navPoint id="{{ bible.vcode }}_{{ bible.bcode }}_index" playOrder="{{ loop.index }}">
      <navLabel><text>{{ bible.name }}</text></navLabel>
      <content src="contents/{{ bible.vcode }}_{{ bible.bcode }}_index.html"/>
    </navPoint>
    {%- endfor %}

  </navMap>
</ncx>
