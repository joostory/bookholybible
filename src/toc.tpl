<html xmlns:epub="http://www.idpf.org/2007/ops" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>성경</title>
  <link rel="stylesheet" type="text/css" href="style/epub.css" />
</head>

<body data-type="book">
  <nav data-type="toc" epub:type="toc" id="toc_{{ vcode }}">
	<ol>

	{% for bible in bibles %}
        <li data-type="chapter"><a href="contents/{{ bible.vcode }}_{{ bible.bcode }}_index.html">{{ bible.name }}</a></li>
    {%- endfor %}

	</ol>
  </nav>
</body>
</html>
