<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html xmlns:epub="http://www.idpf.org/2007/ops" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>{{ title }}</title>
  <link rel="stylesheet" type="text/css" href="../style/epub.css" />
  <link rel="stylesheet" type="text/css" href="../style/index.css" />
</head>

<body data-type="book">
  <section data-type="content" epub:type="index" data-pdf-bookmark="{{ title }}">
    <div class="content" id="{{ id }}_index">
      <h1 data-type="title">{{ title }}</h1>

		<ol data-type="chapter">
		{% for cnum in range(1, chapter_count + 1) -%}
		<li data-type="sect1"><a href="{{ id }}.html#chapter{{cnum}}">{{ cnum }}장</a></li>
		{% endfor %}
		</ol>
    </div>
  </section>
</body>

</html>
